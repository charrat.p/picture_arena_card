package com.cpe.springboot.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.cpe.springboot.Repository.CardRefRepository;
import com.cpe.springboot.card.model.CardModel;
import com.cpe.springboot.card.model.CardReference;
import com.cpe.springboot.card.model.Rarete;
import com.cpe.springboot.card.model.CardType;

@Service
public class CardReferenceService {
	
	@Autowired
	CardRefRepository cardRefRepository;
	

	public List<CardReference> getAllCardRef() 
	{
		List<CardReference> cardRefList = new ArrayList<>();
		cardRefRepository.findAll().forEach(cardRefList::add);
		return cardRefList;
	}

	public CardReference getCardRef(Integer id) 
	{
		Optional<CardReference> cOpt =cardRefRepository.findById(id);
		if (cOpt.isPresent()) 
		{
			return cOpt.get();
		}
		else 
		{
			return null;
		}
	}
	
	public CardReference getCardRefByLabel(String  name) {
		CardReference cardReturn = null;
		List<CardReference> listwithLabel = cardRefRepository.findByName(name);
		if(listwithLabel.size() == 1) {
			cardReturn = listwithLabel.get(0);
		}
		return cardReturn;
	}

	public void addCardRef(CardReference cardRef) 
	{
		cardRefRepository.save(cardRef);
	}

	public void updateCardRef(CardReference cardRef) 
	{
		cardRefRepository.save(cardRef);
	}
	
	public void initCard() {
		CardReference cardInit = new CardReference( "cat", Rarete.SECRETSUPERRARE, CardType.ANIMAL, 10, 3, 1, 1000);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference("dog", Rarete.COMMON, CardType.ANIMAL, 10, 2, 1, 10);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "horse", Rarete.COMMON, CardType.ANIMAL, 8, 4, 1, 10);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "cow", Rarete.COMMON, CardType.ANIMAL, 10, 1, 1, 10);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "pig", Rarete.COMMON, CardType.ANIMAL, 10, 10, 100, 10);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "jeanne", Rarete.RARE, CardType.HISTORIC, 10, 10, 100, 10);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "orion", Rarete.SECRETSUPERRARE, CardType.HISTORIC, 10, 10, 100, 10);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "umbrella", Rarete.RARE, CardType.EQUIPEMENT, 10, 10, 100, 10);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "knife", Rarete.COMMON, CardType.EQUIPEMENT, 0, 3, 0, 100);
		cardRefRepository.save(cardInit);
		cardInit = new CardReference( "spoon", Rarete.COMMON, CardType.EQUIPEMENT, 0, 3, 0, 100);
		cardRefRepository.save(cardInit);
		
	}
	
	/**
	 * Executed after application start
	 */
	@EventListener(ApplicationReadyEvent.class)
	public void doInitAfterStartup() {
		initCard();
	}
}
