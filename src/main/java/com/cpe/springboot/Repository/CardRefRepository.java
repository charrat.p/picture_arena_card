package com.cpe.springboot.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.card.model.CardReference;

public interface CardRefRepository extends CrudRepository<CardReference, Integer> {
	public List<CardReference> findByName(String name);
}
