package com.cpe.springboot.Repository;

import com.cpe.springboot.card.model.CardReference;
import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.card.model.CardModel;

import java.util.List;

public interface CardModelRepository extends CrudRepository<CardModel, Integer> {
    public List<CardModel> findByOwner(Integer idOwner);

}
